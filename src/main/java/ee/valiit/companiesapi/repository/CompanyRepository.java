package ee.valiit.companiesapi.repository;

import ee.valiit.companiesapi.CompaniesapiApplication;
import ee.valiit.companiesapi.model.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CompanyRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    public List<Company> fetchCompanies() {
        List<Company> companies = jdbcTemplate.query("SELECT * FROM company", (row, rowNum) -> {
                    return new Company(
                            row.getInt("id"),
                            row.getString("name"),
                            row.getString("logo")
                    );
                }
        );
        return companies;
    }

    public Company fetchCompany(int id) {
        List<Company> companies = jdbcTemplate.query("SELECT * FROM company WHERE id = ?",
                new Object[]{id}, // Loogeliste sulgude vahel on ? märkide järjestuses olevad elemendid.
                (row, rowNum) -> {
                    return new Company(
                            row.getInt("id"),
                            row.getString("name"),
                            row.getString("logo")
                    );
                }
        );
        // Kaks näidewt kuidas välja kutsuda. Esimene on inline-if

//        return companies.size() > 0 ? companies.get(0) : null;

        // Klassikaline if
        if (companies.size() > 0) {
            return companies.get(0);
        } else {
            return null;
        }
    }

    public void deleteCompany(int id) {
        jdbcTemplate.update("DELETE FROM company WHERE id = ?", id);
    }

    public void addCompany(Company company) {
        String name = company.getName();
        String logo = company.getLogo();
        jdbcTemplate.update("INSERT INTO company (name, logo) VALUES (?, ?)",
        name, logo);
    }

    public void updateCompany(Company company){
        jdbcTemplate.update("UPDATE company SET name = ?, logo = ? WHERE id = ?",
                company.getName(), company.getLogo(), company.getId());
    }
}